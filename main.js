let curTotal = 0;
let buffer = "0"; // Default display showcase

let prevOperator = null;

const calcDisplay = document.querySelector(".calc-numbers");

document.querySelector(".calculator-buttons").addEventListener("click", e => {
  btnClick(e.target.innerHTML);
});

// Recognizing the button value
const btnClick = val => {
  if (isNaN(parseInt(val))) {
    handleSymbol(val);
  } else {
    handleNum(val);
  }

  reRenderDisplay();
};

const handleSymbol = val => {
  switch (val) {
    case "C":
      buffer = "0";
      curTotal = 0;
      prevOperator = null;
      break;
    case "=":
      if (prevOperator === null) {
        return;
      }
      flushOperation(parseInt(buffer));
      buffer = "" + curTotal;
      prevOperator = null;
      curTotal = 0;
      break;
    case "←":
      if (buffer.length === 1) {
        buffer = "0";
      } else {
        buffer = buffer.substring(0, buffer.length - 1); // Delets numbers one by one
      }
      break;
    default:
      handleMath(val);
      break;
  }
};

const handleNum = val => {
  if (buffer === "0") {
    buffer = val;
  } else {
    buffer += val;
  }
};

const handleMath = val => {
  const internalBuffer = parseInt(buffer);

  if (curTotal === 0) {
    curTotal = internalBuffer;
  } else {
    flushOperation(internalBuffer);
  }

  prevOperator = val;

  buffer = "0";
};

const flushOperation = internalBuffer => {
  if (prevOperator === "+") {
    curTotal += internalBuffer;
  } else if (prevOperator === "-") {
    curTotal -= internalBuffer;
  } else if (prevOperator === "x") {
    curTotal *= internalBuffer;
  } else {
    curTotal /= internalBuffer;
  }
};

const reRenderDisplay = () => {
  calcDisplay.value = buffer;
};
